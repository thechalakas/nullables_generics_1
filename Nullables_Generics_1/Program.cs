﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Nullables_Generics_1
{
    class Program
    {
        static void Main(string[] args)
        {
            //declaring an int type variable as nullable
            //by using the question mark after the value type, I am telling the program and compiler
            //that this variable can have a null value assigned to it.
            int? temp;
            temp = null;

            //lets see what the output will be in this case
            Console.WriteLine("Value of temp is {0} ", temp);

            //when you look at the output, you will notice that it will be blank

            //now, lets use a generic
            //I am using it as an int

            generic_types<int> temp_generic_int = new generic_types<int>(10);

            Console.WriteLine("value stored in generic int is {0}", temp_generic_int.return_generic_value());

            //now, using the generic type again, but this time as a double
            generic_types<double> temp_generic_double = new generic_types<double>(1.152);
            Console.WriteLine("value stored in generic double is {0}", temp_generic_double.return_generic_value());

            //now, using the generic type again, but this time as a string
            //note that unlike int and double, string is a referenc type
            //however, I never said that the generic type is struct or class, so I am able to do the 
            //following
            generic_types<string> temp_generic_string = new generic_types<string>("hello");
            Console.WriteLine("value stored in generic string is {0}", temp_generic_string.return_generic_value());

            //now, I am going to use the second generic type
            //the following code (if you uncomment) will give me a error.
            //generic_types_2<string>
            //this is because, I have explicitly said that any uage of generic_types_2 has to be a struct i.e. value type

            //the following wont give a error becuase, int is a value type
            generic_types_2<int> temp_generic2_int = new generic_types_2<int>(10);
            Console.WriteLine("value stored in generic 2 int is {0} ", temp_generic2_int.return_generic_value());

            //this is to stop the console window from dissapearing
            Console.ReadLine();


        }
    }

    //a class of generic type T
    //I am not using any where clauses and that means,
    //I can use this class with value as well as reference types
    class generic_types<T>
    {
        private T temp;

        //constructor
        public generic_types(T sent_value)
        {
            temp = sent_value;
        }

        public T return_generic_value()
        {
            return temp;
        }
    }

    //here is another generic type class but it uses the where constraint
    //by using the where class, I am able to inform that any usage of this generic type
    //must be in the form of a struct
    //that means, the usage of this will be similar to that of a struct which is a value type
    //and hence cannot have a null value to it.
    class generic_types_2<T> where T : struct
    {
        private T temp;

        public generic_types_2(T sent_value)
        {
            temp = sent_value;
        }

        public T return_generic_value()
        {
            return temp;
        }
    }
}
